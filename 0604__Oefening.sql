use ModernWays;

create table Uitleningen
(
Id int auto_increment primary key,
Startdatum date not null,
Einddatum date,
Leden_Id int not null, constraint fk_Uitleningen_Leden foreign key (Leden_Id) references Leden(Id),
Boeken_Id int not null, constraint fk_Uitleningen_Boeken foreign key (Boeken_Id) references Boeken(Id)
);

/*
-- Oplossing leerkracht:
CREATE TABLE Uitleningen (
Leden_Id INT NOT NULL, -- een boek is altijd door iemand uitgeleend en leden
hebben een INT Id
Boeken_Id INT NOT NULL, -- gelijkaardig
StartDatum DATE NOT NULL,
EindDatum DATE,
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Leden_Id) REFERENCES Leden(Id)
...rest zelf doen
*/