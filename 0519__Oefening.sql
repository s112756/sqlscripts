use Modernways;

alter table Liedjes
add column Genre varchar(20);

update Liedjes
set Genre = 'Hard Rock'
where Artiest = 'Led Zeppelin' or Artiest = 'Van Halen';