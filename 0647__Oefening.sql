use aptunes;

drop procedure if exists MockAlbumReleasesLoop;

delimiter $$
use aptunes $$
create procedure MockAlbumReleasesLoop(in extraReleases int)
begin

declare counter int default 0;
declare success bool;

naamLoop: loop

call MockAlbumReleaseWithSuccess(success);

if success = 1 then
set counter = counter + 1;
end if;
if counter = extraReleases then
leave naamLoop;
end if;

end loop;

end $$
delimiter ;






