use Modernways;

alter table Boeken
change column Categorie Categorie varchar(120) char set utf8mb4 NOT NULL;