create user student@localhost
identified by 'ikbeneenstudent';

Grant execute ON procedure aptunes.GetAlbumDuration
to student@localhost;