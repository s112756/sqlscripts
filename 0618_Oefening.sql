use Modernways;

SELECT table_name, is_updatable
FROM information_schema.views
WHERE table_schema = 'ModernWays';

-- OF

SELECT table_name, is_updatable, view_definition FROM information_schema.views
WHERE table_schema = 'ModernWays';