use ModernWays;

select Voornaam, Familienaam from Studenten
where Id = any (select Studenten_Id from Verenigingrollen);

/*
Oplossing leerkracht is met IN ipv = ANY maar geeft hetzelfde resultaat:

select Voornaam, Familienaam from Studenten
where Id in (select Studenten_Id from Verenigingrollen);

*/

