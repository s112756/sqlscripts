use aptunes;

/*
Dag meneer, 

Ik heb geprobeerd eens een gewone en een unieke index te maken.

Normaal gezien moesten we onze tijden er niet bij zetten maar ondanks de full table scan na de aanmaak van beide indexen
verdwenen is, lijken de tijden niet altijd beter te zijn dan voordien.

Mag ik aannemen dat een unieke index steeds beter presteert dan een gewone index? 
blauw = very low cost
groen = low cost
*/

-- VOOR het aanmaken van de index:
-- => full table scan op Genres
-- exec time: 0:00:0.74701060
-- table lock wt: 0:00:0.00148000

create index NaamIdx on Genres(Naam);    -- (deze is identiek met de oplossing van de leerkracht)

-- NA het aanmaken van de gewone index:
-- => geen full table scans meer => groene table
-- exec time: 0:00:0.78943490       => langer dan ZONDER index?
-- table lock wt: 0:00:0.00044900   => korter dan ZONDER index.

create unique index NaamIdx on Genres(Naam);

-- NA het aanmaken van de unieke index:
-- => geen full table scans meer => blauwe table
-- exec time: 0:00:0.77355120       => korter dan de gewone index, maar langer dan ZONDER index?
-- table lock wt: 0:00:0.00084200   => langer dan de gewone index?
