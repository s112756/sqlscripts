use Modernways;

insert into Liedjes (Titel, Artiest, Album, Jaar)
values('Stairways to Heaven', 'Led Zeppelin', 'Led Zeppelin IV', 1971),
	  ('Goud Enough', 'Molly Tuttle', 'Rise', 2017),
      ('Outrage for the Execution of Willie McGee', 'Goodnight, Texas', 'Conductor', 2018),
      ('They Lie', 'Layla Zoe', 'The Lily', 2013),
      ('It Ain\'t You', 'Danielle Nicole', 'Wolf Den', 2015),
      ('Unchained', 'Van Halen', 'Fair Warning', 1981);
      