use ModernWays;

-- Er stond niet bij te ordenen maar heb het gedaan om de namen in alfabetische volgorde te zien.

select distinct Voornaam from Studenten
where Voornaam = any (select Voornaam from Directieleden)
  and Voornaam = any (select Voornaam from Personeelsleden)
order by Voornaam;

/*
Oplossing leerkracht is:
- zonder distinct (maar was mijn eigen keuze)
- zonder order by (maar was mijn eigen keuze)
- IN ipv = any (maar geeft hetzelfde resultaat)

select Voornaam from Studenten
where Voornaam in (select Voornaam from Directieleden)
  and Voornaam in (select Voornaam from Personeelsleden)
*/