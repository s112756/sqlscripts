use ModernWays;

select Titel, Naam from Games
left join Releases on Games.Id = Games_Id
left join Platformen on Platformen.Id = Platformen_Id;


/*
-- OPLOSSING LEERKRACHT (zelfde resultaat maar andere query)
SELECT Games.Titel, Platformen.Naam FROM Games LEFT JOIN -- de “tweede” JOIN  -- haakjes worden altijd eerst uitgewerkt, gebruik ze ter verduidelijking! 
(Platformen INNER JOIN Releases ON Releases.Platformen_Id = Platformen.Id)
ON Releases.Games_Id = Games.Id; 
*/




