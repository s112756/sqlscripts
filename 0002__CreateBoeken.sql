USE Test;

CREATE TABLE Boeken
(
Voornaam VARCHAR(50) CHAR SET utf8mb4,
Familienaam VARCHAR(80) CHAR SET utf8mb4,
Titel VARCHAR(50) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
Verschijningsjaar VARCHAR(4),
Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(1000)
);