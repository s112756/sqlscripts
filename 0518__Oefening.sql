use Modernways;

alter table Huisdieren
add column Geluid varchar(20);

update Huisdieren
set Geluid = 'WAF!'
where Soort = 'hond';

update Huisdieren
set Geluid = 'miauwww...'
where Soort = 'kat';