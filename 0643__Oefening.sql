use aptunes;

delimiter $$
use aptunes $$
create procedure CreateAndReleaseAlbum(IN titel varchar(100), IN bands_Id int)
begin

	start transaction;

	insert into Albums(Titel) 
	values(titel);

	insert into AlbumReleases 
	values( bands_id , LAST_INSERT_ID());

	commit;

end $$
delimiter ;

/*
Volledig identiek met oplossing leerkracht
*/