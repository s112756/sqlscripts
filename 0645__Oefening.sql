use aptunes;

drop procedure if exists MockAlbumReleaseWithSuccess;

delimiter $$
use aptunes $$
create procedure MockAlbumReleaseWithSuccess(out success bool)
begin

declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;

select count(*)
into numberOfAlbums
from Albums;

select count(*)
into numberOfBands
from Bands;

set randomAlbumId = floor(rand() * numberOfAlbums) + 1;
set randomBandId = floor(rand() * numberOfBands) + 1;

if (randomBandId,randomAlbumId) not in 
		(select Bands_Id, Albums_Id from Albumreleases)
then
insert into Albumreleases
values(randomBandId, randomAlbumId);
set success = 1;
else
set success = 0;
end if;

end $$
delimiter ;

-- identiek met oplossing leerkracht



