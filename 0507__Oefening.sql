use Modernways;

create table Metingen
(
Tijdstip datetime NOT NULL,
Grootte smallint unsigned NOT NULL,
Marge float(3, 2) NOT NULL
);