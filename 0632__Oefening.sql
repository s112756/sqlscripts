use ModernWays;

-- Er stond niet bij te ordenen maar heb het gedaan om de namen in alfabetische volgorde te zien.

select Voornaam, Familienaam from Directieleden
where Loon > all (select Loon from Personeelsleden)
order by Voornaam;

/*
Oplossing leerkracht is zonder de order by, maar dat was mijn eigen keuze.
*/

