use ModernWays;

insert into Uitleningen (Startdatum, Vervaldatum, Leden_Id, Boeken_Id)
values ('2019-02-01', '2019-02-15', 3, 1),
	   ('2019-02-16', '2019-03-02', 2, 1),
       ('2019-02-16', '2019-03-02', 2, 5),
       ('2019-05-01', null, 1, 5);
       
/*
-- Oplossing leerkracht:
INSERT INTO Uitleningen (Leden_Id,Boeken_Id,StartDatum,EindDatum)
VALUES
(3,1,’2019-02-01’,’2019-02-15’), -- Lid 3 is Max, boek 1 Norwegian Wood
-- de rest vul je zelf aan
*/