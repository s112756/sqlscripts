use Modernways;

use Modernways;

select Artiest, sum(Aantalbeluisteringen) as 'Totaal aantal beluisteringen' from Liedjes
where length(Artiest) >= 10
group by Artiest
having sum(Aantalbeluisteringen) >= 100;