use ModernWays;

create view GemiddeldeRatings
as
select Boeken_Id, avg(Rating) as Rating from Reviews
group by Boeken_Id;

/*
-- Oplossing leerkracht:
create view GemiddeldeRatings 
as
select reviews.Boeken_Id, avg(reviews.Rating) as Rating from reviews 
group by reviews.Boeken_Id;
*/