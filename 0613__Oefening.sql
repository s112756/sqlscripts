use ModernWays;

create view AuteursBoeken
as
select concat(Voornaam, ' ', Familienaam) as Auteur, Titel from Personen
inner join Publicaties on Personen.Id = Personen_Id
inner join Boeken on Boeken.Id = Boeken_Id;

/*
-- Oplossing leerkracht:
create view AuteursBoeken 
as
select concat( personen.Voornaam,' ', personen.Familienaam ) as Auteur, Titel from boeken 
inner join publicaties on boeken.Id = publicaties.Boeken_Id
inner join personen on publicaties.Personen_Id = personen.Id;
*/

