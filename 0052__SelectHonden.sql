USE ModernWays;
SELECT Naam
FROM Honden
WHERE Geslacht = "mannelijk"
AND Naam IN
  (SELECT Naam
   FROM Honden
   WHERE Geslacht = "vrouwelijk");