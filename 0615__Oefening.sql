use ModernWays;

alter view AuteursBoeken
as
select concat(Voornaam, ' ', Familienaam) as Auteur, Titel, Boeken.Id as Boeken_Id from Personen
inner join Publicaties on Personen.Id = Personen_Id
inner join Boeken on Boeken.Id = Boeken_Id;


/*
-- Oplossing leerkracht:
alter view auteursboeken as
select concat( personen.Voornaam,' ', personen.Familienaam ) as Auteur, Titel, boeken.Id as Boeken_Id from boeken 
inner join publicaties on boeken.Id = publicaties.Boeken_Id
inner join personen on publicaties.Personen_Id = personen.Id;
*/