use Modernways;

select Huisdieren.Naam as 'Naam huisdier', Baasjes.Naam as 'Naam baasje' from Huisdieren
inner join Baasjes on Huisdieren.Id = Baasjes.Huisdieren_Id;