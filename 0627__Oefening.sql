use ModernWays;

-- Er stond niet bij te ordenen maar heb het gedaan om de namen in alfabetische volgorde te zien.

select distinct Voornaam from Studenten
where length(Voornaam) < (select avg(length(Voornaam)) from Studenten)
order by Voornaam;

-- OF

select Voornaam from Studenten
where length(Voornaam) < (select avg(length(Voornaam)) from Studenten)
group by Voornaam
order by Voornaam;

/*
In de oplossing zegt de leerkracht dat hij alle voornamen LANGER dan neemt.
Maar de vraag is KORTER dan...

Ik heb distinct en order by gebruikt, maar dat werd niet gevraagd. (eigen keuze)
*/

