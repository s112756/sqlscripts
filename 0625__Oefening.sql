use aptunes;

/*
VOOR het aanmaken van de index:
exec time: 0.00:0.20313770
table lock wt: 0.00.0.00052600
*/

create index FamilienaamVoornaamIdx on Muzikanten(Familienaam, Voornaam);  -- is identiek met de oplossing van de leerkacht

/*
NA het aanmaken van de index:
exec time: 0.00:0.12334330      => sneller
table lock wt: 0.00.0.00032100  => sneller
*/