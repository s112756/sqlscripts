use aptunes;

drop procedure if exists MockAlbumRelease;

delimiter $$
use aptunes $$
create procedure MockAlbumRelease()
begin

declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;

select count(*)
into numberOfAlbums
from Albums;

select count(*)
into numberOfBands
from Bands;

set randomAlbumId = floor(rand() * numberOfAlbums) + 1;
set randomBandId = floor(rand() * numberOfBands) + 1;

if (randomBandId,randomAlbumId) not in 
		(select * from Albumreleases)
then
insert into Albumreleases
values(randomBandId, randomAlbumId);
end if;

end $$
delimiter ;

-- identiek met oplossing leerkracht





