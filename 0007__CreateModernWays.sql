CREATE DATABASE Test;

USE Test;

CREATE TABLE MijnBoeken
(
Voornaam VARCHAR(50) CHAR SET utf8mb4,
Familienaam VARCHAR(50) CHAR SET utf8mb4,
Titel VARCHAR(50) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
Verschijningsjaar VARCHAR(4),
Uitgeverij VARCHAR(50) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(150) 
);



