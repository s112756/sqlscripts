use aptunes;

drop procedure if exists DemonstrateHandlerOrder;

delimiter $$
use aptunes $$
create procedure DemonstrateHandlerOrder()
begin

declare willekeurig tinyint default 0;

declare continue handler for sqlstate '45002'
select 'State 45002 opgevangen. Geen probleem' Foutmelding1;

declare continue handler for sqlexception
select 'Een algemene fout opgevangen.' Foutmelding2;

set willekeurig = floor(rand() * 3) + 1;

if willekeurig = 1 then
signal sqlstate '45001';
elseif willekeurig = 2 then
signal sqlstate '45002';
else
signal sqlstate '45003';
end if;

end $$
delimiter ;

-- Enige verschil met oplossing leerkracht is dat ik zonder begin/end doe bij handlers.