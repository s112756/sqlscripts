use aptunes;

drop procedure if exists DangerousInsertAlbumreleases;

delimiter $$
use aptunes $$
create procedure DangerousInsertAlbumreleases()
begin

declare numberOfAlbums int default 0;
declare numberOfBands int default 0;

declare randomAlbumId1 int default 0;
declare randomAlbumId2 int default 0;
declare randomAlbumId3 int default 0;

declare randomBandsId1 int default 0;
declare randomBandsId2 int default 0;
declare randomBandsId3 int default 0;

declare randomValue tinyint default 0;

declare exit handler for sqlexception
begin
rollback;
select 'Nieuwe releases konden niet worden toegevoegd.' Foutmelding;
end;

select count(*) 
into numberOfAlbums 
from Albums;

select count(*) 
into numberOfBands 
from Bands;

set randomAlbumId1 = floor(rand() * numberOfAlbums) + 1;
set randomAlbumId2 = floor(rand() * numberOfAlbums) + 1;
set randomAlbumId3 = floor(rand() * numberOfAlbums) + 1;

set randomBandsId1 = floor(rand() * numberOfBands) + 1;
set randomBandsId2 = floor(rand() * numberOfBands) + 1;
set randomBandsId3 = floor(rand() * numberOfBands) + 1;

set randomValue = floor(rand() * 3) + 1;

start transaction;

insert into Albumreleases
values (randomBandsId1, randomAlbumId1),
	   (randomBandsId2, randomAlbumId2);
       
if randomValue = 1 then
signal sqlstate '45000';
end if;

insert into Albumreleases
values (randomBandsId3, randomAlbumId3);

commit;

end $$
delimiter ;