use aptunes;

/*
-- Eerst de ideale prefixlengtes zoeken:

select count(distinct Voornaam) from Muzikanten; 		     -- 651
select count(distinct left(Voornaam, 9)) from Muzikanten;    -- 651

select count(distinct Familienaam) from Muzikanten; 		 -- 988
select count(distinct left(Familienaam, 9)) from Muzikanten; -- 988
*/

create index VoornaamFamilienaamIdx on Muzikanten(Voornaam(9), Familienaam(9));

/*
Mijn versie is identiek met de oplossing van de leerkracht
*/




