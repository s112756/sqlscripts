use Modernways;

create table Huisdieren
(
Naam varchar(100) char set utf8mb4 NOT NULL,
Leeftijd smallint unsigned NOT NULL,
Soort varchar(50) NOT NULL
);