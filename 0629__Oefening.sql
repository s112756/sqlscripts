use ModernWays;

select Id from Studenten
inner join Evaluaties on Id = Studenten_Id
group by Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);

/*
Oplossing leerkracht is anders maar leidt tot hetzelfe resultaat:
(ik heb een join gedaan die overbodig was als ik het zoals hieronder zou doen
maar de opdracht was nochthans om het MET join te doen!)

select Studenten_Id as 'Id' from Evaluaties
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);

*/











