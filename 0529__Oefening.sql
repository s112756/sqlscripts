use Modernways;

insert into Boeken (Familienaam, Titel, Verschijningsjaar, Categorie)
values('?', 'Beowulf', '0975', 'Mythologie' ),    -- het jaar in quotes, anders zal hij het zonder 0 toevoegen
	  ('Ovidus', 'Metamorfosen', 8, 'Mythologie');