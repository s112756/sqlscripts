use Modernways;

alter table Baasjes
add column Huisdieren_Id int,
add constraint fk_Baasjes_Huisdieren foreign key (Huisdieren_Id) references Baasjes(Id);