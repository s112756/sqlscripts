use ModernWays;

select Studenten_Id, Vakken_Id, Cijfer from Evaluaties
where Datum >= all (select Datum from Evaluaties);

-- OF

-- Deze is zoals de oplossing van de leerkracht (maar had ik zelf al gemaakt)
-- (Hij heeft er de kolom datum bij gezet maar dat stond zo niet getoond in de opdracht)
select Studenten_Id, Vakken_Id, Cijfer from Evaluaties
where Datum = (select max(Datum) from Evaluaties);

-- OF

select Studenten_Id, Vakken_Id, Cijfer from Evaluaties
where Datum in (select max(Datum) from Evaluaties);

-- OF

select Studenten_Id, Vakken_Id, Cijfer from Evaluaties
where Datum = any (select max(Datum) from Evaluaties);



