USE ModernWays;

/*
Van deze opgave is nergens een oplossing.
Er is wel een feedbackmail waar enkele dingen stonden vermeld dat hier niet aanwezig zouden zijn maar dat is wél zo...'
*/


/*
1. Er zitten in de tabel Boeken bij de Voornaam + Familienaam veel dubbele tussen.
2. Als we kolommen bijmaken per auteur, moeten we de reeds bestaande kolommen dikwijls mee aanpassen.
   Als we dan 1 kolom zouden vergeten aanpassen, is de databank niet meer correct.
3. Je kan dit oplossen door de Voornaam + Achternaam uit de tabel te halen en in een nieuwe tabel
   Auteurs te steken.
4. Maar wat als de Auteur ook boeken gaat lenen?
   Dan gaan de Voornaam + Achternaam van Auteurs ook in een tabel Ontleners tevoorschijn komen en dus
   weer dubbel vermeld staan in onze databank.
=> We gaan alle Voornaam + Achternaam in een nieuwe tabel Personen steken.
   Zo staan er nooit dubbele Voornaam + Familienaam vernoemd in onze databank.
*/

-- Daarvoor maken we dus eerst de tabel Personen aan, met daarin de kolommen voor de namen:
DROP TABLE IF EXISTS Personen;
CREATE TABLE Personen (
    Voornaam VARCHAR(255) NOT NULL,
    Familienaam VARCHAR(255) NOT NULL
);

/*
Vervolgens gaan we de namen uit de tabel Boeken kopiëren naar de nieuwe tabel Personen.
We gebruiken distinct zodat we geen dubbele namen kopiëren.
Maar dat mag alleen als je weet dat er geen twee *verschillende mensen* zijn die dezelfde 
Voornaam + Achternaam hebben!
Daarom zijn gebruik van keys dikwijls handig, zij zijn uniek en onderscheidend.
*/
INSERT INTO Personen (Voornaam, Familienaam)
   SELECT DISTINCT Voornaam, Familienaam FROM Boeken;
   
/*
1. We voegen nog wat kolommen toe, waarvan een primary key (Id).
2. Als we dan alles tonen uit de tabel Personen, zien we dat elke auteur 1 keer voorkomt en dat ze allemaal een unieke Id hebben.
*/  
ALTER TABLE Personen ADD (
   Id INT AUTO_INCREMENT PRIMARY KEY,
   AanspreekTitel VARCHAR(30) NULL,
   Straat VARCHAR(80) NULL,
   Huisnummer VARCHAR (5) NULL,
   Stad VARCHAR (50) NULL,
   Commentaar VARCHAR (100) NULL,
   Biografie VARCHAR(400) NULL);
   
/*
1. We willen nu de Personen met Boeken linken.
2. We voegen een kolom (Personen_Id) toe bij Boeken die later de foreign keys zal bevatten.
3. Eerst moeten we null zeggen ipv not null. 
   Het systeem verwacht dat je eerst de kolom vult met data, en dan de not null eraan toewijst.
   (Bij mij gaat NOT NULL direct? Ik krijg geen foutmelding zoals gezegd in de theorie.)
*/
ALTER TABLE Boeken ADD Personen_Id INT NULL;

/*
1. We gaan de foreign keys invullen in de tabel Boeken.
2. We gaan de primary keys van Personen (Personen.Id) kopiëren naar de foreign keys van Boeken (Boeken.Personen_Id)
   Daarvoor gaan we tijdens de update joinen met een where-voorwaarde om te zorgen dat er gekopiëerd wordt volgens gewenste vereisten.
*/
UPDATE Boeken CROSS JOIN Personen
    SET Boeken.Personen_Id = Personen.Id
WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam;
    
/*
Nu kunnen we de not null constraint toevoegen aan de foreign key kolom (Personen_Id) van Boeken.
(Bij mij was dat precies niet nodig om pas achteraf NOT NULL toe te voegen?)
*/
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;

/*
Nu zien we bij Boeken dus nog steeds die kolommen Voornaam + Achternaam.
Aangezien we die ergens anders gekopieerd hebben, mogen deze kolommen verwijderd worden.
*/
ALTER TABLE Boeken DROP COLUMN Voornaam,
    DROP COLUMN Familienaam;
    
/*
Nu gaan we van de kolom Personen_Id een echte foreignkey-kolom maken. Dat was nog niet gebeurd.
Voorlopig was het een gewone not null kolom.
Dat doen we door de constraint toe te voegen:
*/
ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Personen
   FOREIGN KEY(Personen_Id) REFERENCES Personen(Id);
   



   

