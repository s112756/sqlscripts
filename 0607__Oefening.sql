use Modernways;

select Omschrijving from Taken
left join Leden on Leden_Id = Leden.Id
where Leden.Id is null;

/*
-- Oplossing leerkracht:
SELECT Omschrijving FROM Taken 
LEFT JOIN Leden ON Taken.Leden_Id = Leden.Id 
WHERE Leden.Id IS NULL 
*/
