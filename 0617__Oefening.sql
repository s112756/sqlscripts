use ModernWays;

create view AuteursBoekenRatings
as
select Auteur, Titel, Rating from AuteursBoeken
inner join GemiddeldeRatings on AuteursBoeken.Boeken_Id = GemiddeldeRatings.Boeken_Id;

/*
-- Oplossing leerkracht:
create view AuteursBoekenRatings
as
select Auteur, Titel, Rating from auteursboeken 
inner join gemiddelderatings on auteursboeken.Boeken_Id = gemiddelderatings.Boeken_Id;
*/