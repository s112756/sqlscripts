use aptunes;

drop procedure if exists CleanupOldMemberships;

delimiter $$
use aptunes $$
create procedure CleanupOldMemberships(IN gekozenDatum date, OUT aantalVerwijderd int )
begin

start transaction;

select count(*) 
into aantalVerwijderd
from Lidmaatschappen
where Einddatum < gekozenDatum;

delete from Lidmaatschappen
where Einddatum < gekozenDatum;

commit;

end $$
delimiter ;

CALL CleanupOldMemberships('2000-01-01', @numberCleaned);
SELECT @numberCleaned;

/*
In feedbackmail krijg ik de opmerking dat ik geen transactie gebruikt heb.
Maar er staat ook bij dat dat niet per se fout is omdat dat niet gevraagd werd in de opdracht.
Maar het is wel aan te raden dus heb ik het erbij gezet.
*/

/*
-- Oplossing leerkracht:

delimiter $$
create procedure CleanupOldMemberships(IN teKiezenDatum date, OUT gewist int )
begin

start transaction;

	select count(*) 
	into gewist 
	from Lidmaatschappen
    where Einddatum is not null and Einddatum < teKiezenDatum; -- de not null is volgens mij overbodig omdat deze automatisch niet meegerekend worden.
															   -- ik heb dit getest en zoals in de opdracht ook vermeld staat, "Lidmaatschappen met einddatum NULL blijven sowieso staan."
	delete from Lidmaatschappen
	where Einddatum is not null and Einddatum < teKiezenDatum; -- ook hier is de not null overbodig volgens mij.

commit;
end $$
delimiter ;
*/











