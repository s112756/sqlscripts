use aptunes;

drop procedure if exists GetAlbumDuration;

delimiter $$
use aptunes $$
create procedure GetAlbumDuration(in albumsId int , out totalDuration smallint unsigned)
begin

declare songDuration tinyint unsigned default 0;
declare ok tinyint default 0;

declare naamCursor cursor for
select Lengte from Liedjes
where Albums_Id = albumsId;

declare continue handler for not found
set ok = 1;

set totalDuration = 0;

open naamCursor;
naamLoop: loop

fetch naamCursor into songDuration;

if ok = 1 then
leave naamLoop;
end if;

set totalDuration = totalDuration + songDuration;

end loop;
close naamCursor;

end $$
delimiter ;