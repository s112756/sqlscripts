use sportclub;

alter table Leden
add column Taken_Id int,
add constraint fk_Leden_Taken foreign key (Taken_Id) references Taken(Id);