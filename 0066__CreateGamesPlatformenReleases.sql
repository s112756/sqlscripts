use modernways;

create table Games2
(
Id int auto_increment primary key,
Titel varchar(50) char set utf8mb4 not null
);

create table Platformen2
(
Id int auto_increment primary key,
Naam varchar(50) char set utf8mb4 not null
);

create table Releases2
(
Id int auto_increment primary key,
Games2_Id int, constraint fk_Releases2_Games2 foreign key (Games2_Id) references Games2(Id),
Platformen2_Id int, constraint fk_Releases2_Platformen2 foreign key (Platformen2_Id) references Platformen2(Id)
);