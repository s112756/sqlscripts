use aptunes;

/*
-- VOOR de index aangemaakt werd:
-- exec time: 0:00:0.22328650
-- table lock wt: 0:00:0.00019100
*/

create index TitelIdx on Liedjes(Titel);  -- is identiek met de oplossing van de leerkracht

-- NADAT de index aangemaakt werd:
-- exec time: 0:00:0.17815430       => sneller
-- table lock wt: 0:00:0.00019100   => hetzelfde?