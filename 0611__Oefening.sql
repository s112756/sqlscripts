use ModernWays;

select Titel, 'Geen platformen gekend' as Naam from Games  -- het zijn de bovenste kolommen die in beeld komen als titel van de kolommen.
left join Releases on Id = Games_Id
where Games_Id is null

union all

select 'Geen games gekend', Naam from Platformen  -- Alias bij eerste kolom hier MAG maar doet toch niks, dus kan weggelaten worden.
left join Releases on Id = Platformen_Id
where Platformen_Id is null;


/*
-- Oplossing leerkracht:
SELECT Games.Titel, ‘Geen platformen gekend’ FROM Games  --leerkracht is alias vergeten as ...
LEFT JOIN Releases ON Games.Id = Releases.Games_Id
WHERE Releases.Games_Id IS NULL
-- vul zelf aan
*/


