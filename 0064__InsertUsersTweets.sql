use modernways;

insert into Users2 (Handle)
values ('NintendoEurope'), 
	   ('Xbox');

insert into Tweets2 (Bericht, Users_Id)
values ('Don''t forget -- Nintendo Labo: VR Kit launches 12/04!',1),
	   ('Splat it out in the #Splatoon2 EU Community Cup 5 this Sunday!',1),
	   ('Crikey! Keep an eye out for cardboard crocs and other crafty wildlife on this jungle train ride! #Yoshi',1),
       ('You had a lot to say about #MetroExodus. Check out our favorite 5-word reviews.',2),
       ('It''s a perfect day for some mayhem.',2),
       ('Drift all over N. Sanity Beach and beyond in Crash Team Racing Nitro-Fueled.',2)