USE ModernWays;

INSERT INTO Personen (Voornaam, Familienaam)
VALUES ('Jean-Paul', 'Sartre');

INSERT INTO Boeken (Titel, Stad, Verschijningsdatum, Commentaar, Categorie, Personen_Id)
VALUES ('De Woorden', 'Antwerpen', '1962', 'Een zeer mooi boek.', 'Roman',
					(SELECT Id FROM Personen 
                     WHERE Familienaam = 'Sartre' AND Voornaam = 'Jean-Paul')
	   );
       
       
/*
Feedbackmail zegt enkel dat ModernWays met kleine letter was. Voor de rest niks.
Dus neem aan dat dit juist was.
*/