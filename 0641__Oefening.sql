use aptunes;

delimiter $$
use aptunes $$
create procedure NumberOfGenres(OUT aantalGenres tinyint)
begin 
	select count(distinct Naam) 
	into aantalGenres
	from Genres;
end $$
delimiter ;

/*
Leerkracht heeft count(*) ipv count(distinct Naam) maar ik krijg hetzelfde resultaat.
Feedbackmail geeft daarom geen foutmelding over deze oefening.
*/



