use ModernWays;

-- Er stond niet bij te ordenen maar heb het gedaan om de namen in alfabetische volgorde te zien.

select Voornaam, Familienaam from Directieleden
where Loon < any (select Loon from Personeelsleden)
order by Voornaam;

/*
Oplossing leerkracht was zonder order by, maar dat was mijn eigen keuze.
*/